Make your next wedding, party or corporate event an affair to remember with special event rentals from Gervais Party & Tent Rentals. We carry an incredible selection of over 1,200 high quality party supplies for rent, including lounge furniture and tents.


Address: 75 Milner Ave, Scarborough, ON M1S 3P6, Canada

Phone: 416-288-1846

Website: https://gervaisrentals.com
